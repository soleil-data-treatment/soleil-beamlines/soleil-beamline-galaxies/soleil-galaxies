# Aide et ressources de Scripts GALAXIES pour Synchrotron SOLEIL

## Résumé
- Acquisition pour les instruments, photo émission, traitement et pré traitement des données, analyse des images, convertir images en donnée exploitable, calcul nombre de Bragg, faire fonctionner les instruments
- Créé à Synchrotron Soleil

## Navigation rapide
| Wiki SOLEIL |
| ------ |
| [Tutoriaux](https://gitlab.com/soleil-data-treatment/soleil-galaxies/-/wikis/home) |

## Sources
- Code source: Oui, sur un dépôt ouvert en ligne (type Git, SourceForge, ...)
- Documentation officielle: Oui

## Installation
- Systèmes d'exploitation supportés: Windows, Linux, MacOS, Machine virtuelle
- Installation: Facile (tout se passe bien)

## Format de données
- en entrée: nexus
- en sortie: ascii, nexus
- sur un disque dur, sur la Ruche